Hello, I'm J. B. Crawford, a Senior Professional Services Engineer at GitLab. I use they/them pronouns.

## Work Style

I'm located in Albuquerque, New Mexico, USA, in the mountain time zone. That's either UTC-7 or UTC-6 depending on the season because we do a weird thing about that here. I usually work about 9-5, but you're more likely to get my attention after 5 than before 9. Slack is a good way to reach me but it seems kind of weird about notifications on my phone, so if you're trying to get my attention it's not a bad idea to email or text me. That ought to be a push notification for sure.

Because of my client situation right now I'm using more than one laptop and more than one calendar, which makes me especially prone to forgetting meetings. Sorry. Please always invite my GitLab email to meetings because that's the calendar I keep the closest eye on.

## About Me (Professional)

I started my career in information security, and that's where my academic qualifications are. Early in my career I did incident response and focused on network forensics. I love to look at pcaps! During my time working with the federal government, though, I started to shift towards DevOps and DevSecOps. Part of this is because I love "old-school system administration" and focus a lot on Linux fundamentals, and my network expertise is very useful for complex cloud architectures. After I jumped ship to the private sector I did DevOps work in the entertainment and healthcare industries, before I started shifting towards professional services for a health tech company and then came to GitLab.

Because of my career background I'm particularly interested in DevOps work around security and compliance. I know a lot about federal and defense compliance issues and a bit about healthcare. I'm also particularly interested in authentication/authorization, SSO and directory services, that kind of thing.

## About Me (Personal)

I'm an amateur historian of computer, telecommunications, and cold war history, particularly the intersection of the three. I am known to some for my blog on these topics, [Computers Are Bad](https://computer.rip). When I'm on not in the office I'm often on a road trip somewhere in the American Southwest, or occasionally as far north as BC and Alberta. Those are longer drives.

My husband is a doctoral student in computer science, and our cat doesn't do anything that ambitious. I like to work on small electric vehicles and old video projectors but try not to collect them. I do collect small/medium-business telephone systems.